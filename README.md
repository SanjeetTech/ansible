# Ansible

Ansible in Docker Container based on Alpine Linux

https://gitlab.com/epicdocker/ansible/container_registry

| Tag               | Ansible package version  | Alpine version   |
|-------------------|--------------------------|------------------|
| `edge`            | >= 6.4.0-r0              | edge             |
| `latest`          | >= 5.8.0-r0              | latest (>= 3.16) |
| `2.13.0`          | 5.8.0-r0                 | 3.16.2           |
| `2.11.6`          | 4.8.0-r0                 | 3.15.0           |
| `2.10.5`          | 2.10.7-r0                | 3.14.0           |
| `2.10.4`          | 2.10.5-r0                | 3.13.0           |
| `2.9.14`          | 2.9.14-r0                | 3.12.1           |
| `2.9.7`           | 2.9.7-r0                 | 3.11.6           |
| `2.8.11`          | 2.8.11-r0                | 3.10.5           |
| `2.7.17`          | 2.7.17-r0                | 3.9.6            |
| `2.6.20`          | 2.6.20-r0                | 3.8.5            |

`edge` and `latest` are built automatically every week

Table from 19 September 2022
