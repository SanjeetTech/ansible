ARG IMAGE_TAG=latest
FROM registry.gitlab.com/epicdocker/ansible:$IMAGE_TAG

#### https://galaxy.ansible.com/community/hashi_vault
#### https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/hashi_vault_lookup.html
RUN ansible-galaxy collection install community.hashi_vault

#### https://github.com/TerryHowe/ansible-modules-hashivault
#### https://terryhowe.github.io/ansible-modules-hashivault/modules/list_of_hashivault_modules.html
RUN ansible-galaxy install 'git+https://github.com/TerryHowe/ansible-modules-hashivault.git' \
 && pip install ansible-modules-hashivault
